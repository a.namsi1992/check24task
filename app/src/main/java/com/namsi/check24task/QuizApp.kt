package com.namsi.check24task

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class QuizApp : Application() {
}
package com.namsi.check24task.di

import com.namsi.question_interactor.GetQuestions
import com.namsi.question_interactor.QuestionsInteractors
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object QuestionInteractorModule {

    @Provides
    @Singleton
    fun provideQuestionsInteractors(): QuestionsInteractors =
        QuestionsInteractors.build()

    @Provides
    @Singleton
    fun provideGetQuestions(questionsInteractors: QuestionsInteractors): GetQuestions =
        questionsInteractors.getQuestions
}
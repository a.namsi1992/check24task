package com.namsi.question_interactor

import com.namsi.question_interactor.mappers.QuestionMapper
import com.namsi.questions_remote_datasource.APIServices
import com.namsi.questions_remote_datasource.repositories.QuestionsRemoteRepositories

data class QuestionsInteractors(
    val getQuestions: GetQuestions
) {
    companion object {
        fun build(): QuestionsInteractors {
            return QuestionsInteractors(
                getQuestions = GetQuestions(QuestionsRemoteRepositories(APIServices.build()), QuestionMapper())
            )
        }
    }
}

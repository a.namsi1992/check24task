package com.namsi.question_interactor

import com.namsi.core.DataState
import com.namsi.question_interactor.mappers.QuestionMapper
import com.namsi.questiondomain.Question
import com.namsi.questions_remote_datasource.repositories.QuestionsRemoteRepositories
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class GetQuestions(
    private val questionsRemoteRepositories: QuestionsRemoteRepositories,
    private val questionMapper: QuestionMapper
) {
    fun execute(): Flow<DataState<List<Question>>> = flow {
        try {
            emit(DataState.Loading)
            val questions: List<Question> = try {
                questionMapper.mapList(questionsRemoteRepositories.getQuestions())
            } catch (e: Exception) {
                emit(DataState.Error(e))
                listOf()
            }
            emit(DataState.Success(questions))
        } catch (e: Exception) {
            e.printStackTrace()
            emit(DataState.Error(e))
        }
    }
}
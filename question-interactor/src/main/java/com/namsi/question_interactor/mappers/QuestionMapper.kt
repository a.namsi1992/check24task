package com.namsi.question_interactor.mappers

import com.namsi.core.BaseMapper
import com.namsi.questiondomain.Answer
import com.namsi.questiondomain.Question
import com.namsi.questions_remote_datasource.dto.QuestionDto

class QuestionMapper : BaseMapper<QuestionDto, Question> {
    override fun map(from: QuestionDto): Question {
        return Question(
            question = from.question,
            questionImageUrl = from.questionImageUrl,
            correctAnswer = from.correctAnswer,
            score = from.score,
            answers = from.answers.toList().map { Answer(it.first, it.second) }
        )
    }
}
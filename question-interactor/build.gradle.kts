apply {
    from("$rootDir/library-build.gradle")
}

dependencies {
    "implementation"(project(Modules.core))
    "implementation"(project(Modules.questionsRemoteDatasource))
    "implementation"(project(Modules.questionDomain))

    "implementation"(Kotlinx.coroutinesCore)
}
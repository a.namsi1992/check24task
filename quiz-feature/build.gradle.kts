apply {
    from("$rootDir/android-library-build.gradle")
}



dependencies {
    "implementation"(project(Modules.core))
    "implementation"(project(Modules.questionDomain))
    "implementation"(project(Modules.questionInteractor))

    "implementation"(Glide.glide)
    "implementation"(Glide.glideCompiler)
    "implementation"(Google.navigationFragment)
    "implementation"(Google.navigationUi)
    "implementation"(Google.navigationUi)
}
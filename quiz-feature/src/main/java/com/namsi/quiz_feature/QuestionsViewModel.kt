package com.namsi.quiz_feature

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.namsi.core.DataState
import com.namsi.question_interactor.GetQuestions
import com.namsi.questiondomain.AnswerStatus
import com.namsi.questiondomain.Question
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class QuestionsViewModel @Inject constructor(
    private val getQuestions: GetQuestions
) : ViewModel() {

    private val _questionsListStateFlow = MutableStateFlow<DataState<List<Question>>>(DataState.Success(ArrayList()))
    val questionsListStateFlow = _questionsListStateFlow

    init {
        getQuestions()
    }

    private fun getQuestions() {
        getQuestions.execute().onEach {
            _questionsListStateFlow.emit(it)
        }.launchIn(viewModelScope)
    }

    var questionsList: ArrayList<Question> = ArrayList()
    var score = 0
    var bestScore = MutableStateFlow<Int>(0)
    var currentQuestionIndex = MutableStateFlow<Int>(0)
    var numberOfQuestions = 0
    fun getCurrentQuestion(): Question? {
        return try {
            questionsList[currentQuestionIndex.value]
        } catch (e: Exception) {
            return null
        }
    }

    fun submitAnswer(isRight: Boolean) {
        viewModelScope.launch {
            if (isRight) {
                score += getCurrentQuestion()!!.score
            }
            currentQuestionIndex.emit(currentQuestionIndex.value + 1)
        }
    }

    fun endGame() {
        viewModelScope.launch {
            if (score > bestScore.value) bestScore.emit(score)
            resetAnswers()
            score = 0
            currentQuestionIndex.value = 0
        }
    }

    private fun resetAnswers() {
        for (question in questionsList)
            for (answer in question.answers)
                answer.status = AnswerStatus.UNSELECTED
    }

}
package com.namsi.quiz_feature.questions

import android.annotation.SuppressLint
import androidx.recyclerview.widget.RecyclerView
import com.namsi.questiondomain.Answer
import com.namsi.questiondomain.AnswerStatus
import com.namsi.quiz_feature.R
import com.namsi.quiz_feature.databinding.AnswerItemBinding

class AnswersListViewHolder(private val answerItemBinding: AnswerItemBinding) :
    RecyclerView.ViewHolder(answerItemBinding.root) {
    @SuppressLint("SetTextI18n")
    fun bind(answer: Answer) {
        answerItemBinding.answerText.text = answer.value
        answerItemBinding.answerText.setBackgroundResource(
        when(answer.status){
            AnswerStatus.UNSELECTED -> R.drawable.answer_background
            AnswerStatus.WRONG -> R.drawable.wrong_answer_background
            AnswerStatus.RIGHT -> R.drawable.right_answer_background
        })
    }
}
package com.namsi.quiz_feature.questions

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.namsi.questiondomain.Answer
import com.namsi.quiz_feature.databinding.AnswerItemBinding

class AnswersAdapter (private val answersList: List<Answer>, var clickListener: (Int) -> Unit) :
    RecyclerView.Adapter<AnswersListViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnswersListViewHolder {
        val itemBinding = AnswerItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AnswersListViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: AnswersListViewHolder, position: Int) {
        holder.bind(answersList[position])
        holder.itemView.setOnClickListener {
            clickListener(position)
        }
    }

    override fun getItemCount(): Int = answersList.size
}
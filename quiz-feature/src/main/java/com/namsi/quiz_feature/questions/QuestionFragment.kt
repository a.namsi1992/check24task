package com.namsi.quiz_feature.questions

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.namsi.questiondomain.Answer
import com.namsi.questiondomain.AnswerStatus
import com.namsi.quiz_feature.QuestionsViewModel
import com.namsi.quiz_feature.R
import com.namsi.quiz_feature.databinding.QuestionFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class QuestionFragment : Fragment(R.layout.question_fragment) {

    private var _binding: QuestionFragmentBinding? = null
    private val binding get() = _binding!!

    private val questionsViewModel: QuestionsViewModel by activityViewModels()

    private lateinit var answersAdapter: AnswersAdapter
    private var answersList = ArrayList<Answer>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = QuestionFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        answersAdapter = AnswersAdapter(answersList) { position ->
            selectAnswer(position)
        }
        binding.answersRecyclerView.layoutManager = LinearLayoutManager(context)
        binding.answersRecyclerView.adapter = answersAdapter
        initQuestion()
        initObservables()
    }

    private fun selectAnswer(position: Int) {
        val currentQuestion = questionsViewModel.getCurrentQuestion()
        if (currentQuestion != null) {
            currentQuestion.answers[position].status =
                if (currentQuestion.answers[position].id == currentQuestion.correctAnswer) {
                    AnswerStatus.RIGHT
                } else {
                    AnswerStatus.WRONG
                }
            answersAdapter.notifyItemChanged(position)
            answersAdapter.clickListener = {}
            questionsViewModel.submitAnswer(currentQuestion.answers[position].status == AnswerStatus.RIGHT)
        }
    }

    private fun initObservables() {
        lifecycleScope.launchWhenResumed {
            questionsViewModel.currentQuestionIndex.collect {
                delay(2000)
                initQuestion()
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun initQuestion() {
        val currentQuestion = questionsViewModel.getCurrentQuestion()
        if (currentQuestion != null) {
            binding.questionHeaderTextView.text = resources.getString(
                R.string.question_header_text,
                questionsViewModel.currentQuestionIndex.value+1,
                questionsViewModel.numberOfQuestions,
                questionsViewModel.score
            )
            binding.questionScoreTextView.text =
                resources.getString(R.string.question_score_text, currentQuestion.score ?: 0)

            binding.questionImage.loadImage(currentQuestion.questionImageUrl)
            binding.questionText.text = currentQuestion.question

            answersList.clear()
            answersList.addAll(currentQuestion.answers)
            answersAdapter.clickListener = { selectAnswer(it) }
            answersAdapter.notifyDataSetChanged()

        } else {
            findNavController().navigateUp()
            questionsViewModel.endGame()
        }

    }

}

private fun ImageView.loadImage(url: String?) {
    Glide.with(context)
        .load(url)
        .placeholder(R.drawable.check24_logo_white)
        .error(R.drawable.check24_logo_white)
        .into(this)
}

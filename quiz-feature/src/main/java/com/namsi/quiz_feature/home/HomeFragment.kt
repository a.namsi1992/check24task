package com.namsi.quiz_feature.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.namsi.core.DataState
import com.namsi.questiondomain.Question
import com.namsi.quiz_feature.QuestionsViewModel
import com.namsi.quiz_feature.R
import com.namsi.quiz_feature.databinding.HomeFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class HomeFragment : Fragment(R.layout.home_fragment) {

    private var _binding: HomeFragmentBinding? = null
    private val binding get() = _binding!!

    private val questionsViewModel: QuestionsViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = HomeFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
        binding.startBtn.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_questionFragment)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun initObservers() {
        lifecycleScope.launchWhenResumed {
            questionsViewModel.bestScore.collect {
                binding.highscoreTextView.text = resources.getString(R.string.highscore, it)
            }
        }
        lifecycleScope.launchWhenResumed {
            questionsViewModel.questionsListStateFlow.collect {
                when (it) {
                    is DataState.Error -> {
                        Log.e("questionLoading", " ERROR: ${it.exception.message}")
                        binding.startBtn.isEnabled = false
                    }
                    DataState.Loading -> {
                        Log.e("questionLoading", " loading")
                        binding.startBtn.isEnabled = false
                    }
                    is DataState.Success -> {
                        Log.e("questionLoading", " Success" + it.data?.size)
                        questionsViewModel.numberOfQuestions = it.data?.size ?: 0
                        questionsViewModel.questionsList = it.data as ArrayList<Question>
                        if (questionsViewModel.numberOfQuestions != 0)
                            binding.startBtn.isEnabled = true
                    }
                }
            }

        }
    }
}
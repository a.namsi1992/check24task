object Android {
    const val appId = "com.namsi.check24task"
    const val compileSdk = 31
    const val minSdk = 21
    const val targetSdk = 31
    var versionCode = 1
    var versionName = "1.0"
}
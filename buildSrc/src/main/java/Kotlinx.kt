object Kotlinx {

    private const val coroutinesCoreVersion = "1.6.0-RC"
    const val coroutinesCore = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesCoreVersion"

}
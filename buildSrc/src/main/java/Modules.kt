object Modules {

    const val app = ":app"
    const val core = ":core"
    const val questionsRemoteDatasource = ":questions-remote-datasource"
    const val questionDomain = ":question-domain"
    const val questionInteractor = ":question-interactor"
    const val quizFeature = ":quiz-feature"

}
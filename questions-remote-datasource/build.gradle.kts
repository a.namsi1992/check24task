
apply{
    from("$rootDir/library-build.gradle")
}

dependencies{
    "implementation" (Retrofit.retrofit)
    "implementation" (Retrofit.gson)
}
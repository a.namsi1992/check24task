package com.namsi.questions_remote_datasource

object APIConstants {

    internal const val API_BASE_URL = "https://app.check24.de/vg2-quiz/"

    internal const val GET_QUIZ_PATH = "quiz.json"


}
package com.namsi.questions_remote_datasource.repositories

import com.namsi.questions_remote_datasource.APIServices
import com.namsi.questions_remote_datasource.dto.QuestionDto

class QuestionsRemoteRepositories(private val apiServices: APIServices) {
    suspend fun getQuestions(): List<QuestionDto> {
        return apiServices.getQuestions().questions
    }

}
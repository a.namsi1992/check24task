package com.namsi.questions_remote_datasource.dto

data class QuestionDto(
    val question: String,
    val questionImageUrl: String?,
    val correctAnswer: String,
    val score: Int,
    val answers: Map<String, String>
)

package com.namsi.questions_remote_datasource.dto

data class BaseResponse(
    val questions: List<QuestionDto>
)

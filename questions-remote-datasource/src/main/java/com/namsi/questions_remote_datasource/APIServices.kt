package com.namsi.questions_remote_datasource

import com.google.gson.GsonBuilder
import com.namsi.questions_remote_datasource.APIConstants.API_BASE_URL
import com.namsi.questions_remote_datasource.APIConstants.GET_QUIZ_PATH
import com.namsi.questions_remote_datasource.dto.BaseResponse
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface APIServices {

    @GET(GET_QUIZ_PATH)
    suspend fun getQuestions(): BaseResponse

    companion object{
        fun build() : APIServices{
            return provideRetrofit().create(APIServices::class.java)
        }

        private fun provideRetrofit(): Retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .baseUrl(API_BASE_URL)
            .build()
    }
}
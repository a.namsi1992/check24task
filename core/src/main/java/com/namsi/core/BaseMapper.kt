package com.namsi.core

interface BaseMapper<FROM, TO> {
    fun map(from: FROM): TO

    fun mapInverse(from: TO): FROM = TODO("Must be implemented before using it")

    fun mapList(from: List<FROM>): List<TO> {
        return ArrayList<TO>().also { models ->
            from.forEach { item ->
                models.add(map(item))
            }
        }
    }

    fun mapListInverse(from: List<TO>): List<FROM> {
        return ArrayList<FROM>().also { models ->
            from.forEach { item ->
                models.add(mapInverse(item))
            }
        }
    }
}
package com.namsi.core

sealed class DataState<out R> {
    data class Success<T>(
        val data: T? =null
    ): DataState<T>()

    data class Error(
        val exception: Exception
    ): DataState<Nothing>()

    object Loading: DataState<Nothing>()

}
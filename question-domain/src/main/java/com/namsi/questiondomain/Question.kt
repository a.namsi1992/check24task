package com.namsi.questiondomain

data class Question(
    val question: String,
    val questionImageUrl: String?,
    val correctAnswer: String,
    val score: Int,
    val answers: List<Answer>
)
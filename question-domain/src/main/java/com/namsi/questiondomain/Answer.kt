package com.namsi.questiondomain

data class Answer(
    val id: String,
    val value : String
){
    var status: AnswerStatus = AnswerStatus.UNSELECTED
}

enum class AnswerStatus {
    UNSELECTED, WRONG, RIGHT
}
